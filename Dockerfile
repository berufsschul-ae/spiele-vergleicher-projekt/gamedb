FROM openjdk:11-slim AS build

COPY . build/
WORKDIR /build/
RUN ./mvnw clean install -Dmaven.test.skip=true --batch-mode


FROM openjdk:11-slim

COPY --from=build /build/target/*.jar /app/gameDB.jar
WORKDIR /app/

EXPOSE 8080
CMD ["java","-jar", "gameDB.jar"]