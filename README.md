# Spiele-Vergleicher GameDB
[![coverage report](https://gitlab.com/berufsschul-ae/spiele-vergleicher-gamedb/badges/develop/coverage.svg)](https://gitlab.com/berufsschul-ae/spiele-vergleicher-gamedb/-/commits/develop)

Die Game-DB für das Spiele-Vergleicher-Projekt. 
Das Projekt bietet eine REST-API mit spiele Daten an.
Diese ermöglicht einem Spiele in der DB nach Titel suchen und mit der ID zu finden, 
außerdem gibt es noch einige administrative Endpoints, um die Date von der DB zu manipulieren.  

## Installation

Benötigt wird `java 11` oder `docker`. Empfohlen wird `docker-compose`, um das aufsetzen zu vereinfachen.

### Java Setup
Um das Programm mit Java zu starten, nutze den Maven wrapper aus dem Projekt:

`./mvnw install` generiert eine JAR Datei im `./target` Verzeichnis. 
Du kannst aber die Applikation auch einfach mit `./mvnw spring-boot:run` starten.
Über `localhost:8080` kannst du auf die Api zugreifen.

### Docker Setup
Mit Docker kannst du aus dem Projekt ein eigenes Image bauen, oder du nutzt das aus dem Repository.
Das Image kannst du mit `docker pull registry.gitlab.com/berufsschul-ae/spiele-vergleicher-gamedb/gamedb:latest` dir herunterladen und 
mit `docker run -p 8080:8080 registry.gitlab.com/berufsschul-ae/spiele-vergleicher-gamedb/gamedb:latest` anschließend starten.

### Docker-Compose Setup
Das Projekt bring schon eine fertige docker-compose Datei mit. 
Starten tust du sie mit `docker-compose up`. Der Server startet dann auf Port `8080`.

## Dokumentation
Sobald der Server läuft, kann man sich unter `/swagger-ui.html` die Api Dokumentation anschauen.
