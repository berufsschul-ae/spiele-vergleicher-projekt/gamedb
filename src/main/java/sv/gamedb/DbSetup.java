package sv.gamedb;

import lombok.extern.log4j.Log4j2;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import sv.gamedb.model.Game;
import sv.gamedb.reposetory.GameRepository;

import java.util.Arrays;

@Log4j2
@Component
public class DbSetup implements CommandLineRunner {

    private final GameRepository gameRepository;

    public DbSetup(GameRepository gameRepository) {
        this.gameRepository = gameRepository;
    }

    @Override
    public void run(String... args) {

        log.info("Setup the DB...");
//        var games = Arrays.asList(
//                new Game("Rainbow Six Siege", "FPS"),
//                new Game("League of Legends", "Moba"),
//                new Game("UNO", "Card game"),
//                new Game("Monopoly", "Classic")
//        );
//
//        gameRepository.saveAll(games);
//
//        log.info("All saved");
//        log.info("Checking for games...");
//        log.info("Number of games: {}", gameRepository.count());
//        log.info("===============================================");
//        gameRepository.findAll().forEach(game -> log.info("{}", game.toString()));
//        log.info("===============================================");
//        log.info("Finished 0.0");
    }
}
