package sv.gamedb.controller;

import lombok.extern.log4j.Log4j2;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import sv.gamedb.model.Game;
import sv.gamedb.reposetory.GameRepository;

import java.util.List;
import java.util.Optional;

@Log4j2
@CrossOrigin("*")
@RestController()
public class GameController {

    private final GameRepository gameRepository;

    public GameController(GameRepository gameRepository) {
        this.gameRepository = gameRepository;
    }

    @GetMapping(value = "/games", produces = "application/json")
    public ResponseEntity<List<Game>> getGamesBy(@RequestParam(defaultValue = "0") int page, @RequestParam(defaultValue = "10") int size) {
        var requestedPage = PageRequest.of(page, size);
        return ResponseEntity.status(HttpStatus.OK).body(gameRepository.findAll(requestedPage).toList());
    }

    @GetMapping(value = "/games/search", produces = "application/json")
    public ResponseEntity<List<Game>> searchGamesBy(@RequestParam String title) {
        return ResponseEntity.status(HttpStatus.OK).body(gameRepository.findByTitleIgnoreCaseContaining(title));
    }

    @GetMapping(value = "/game/{id}", produces = "application/json")
    public ResponseEntity<Game> getGameBy(@PathVariable long id) {
        var game = gameRepository.findById(id);
        return game.map(value -> ResponseEntity.status(HttpStatus.OK).body(value)).orElseGet(() -> ResponseEntity.status(HttpStatus.NOT_FOUND).body(null));
    }

    @PostMapping(value = "/admin/game", produces = "application/json")
    public ResponseEntity<Game> addGameBy(@RequestParam String title, @RequestParam String description) {
        var game = new Game(title, description);
        log.info("{}", game);
        return ResponseEntity.status(HttpStatus.CREATED).body(gameRepository.save(game));
    }

    @PatchMapping(value = "/admin/game/{id}", produces = "application/json")
    public ResponseEntity<Game> updateGame(@PathVariable long id, @RequestParam Optional<String> title, @RequestParam Optional<String> description) {
        var foundGame = gameRepository.findById(id);
        if (foundGame.isEmpty()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
        }
        var game = foundGame.get();
        game.setTitle(title.orElse(game.getTitle()));
        game.setDescription(description.orElse(game.getDescription()));
        return ResponseEntity.status(HttpStatus.OK).body(gameRepository.save(game));
    }

    @DeleteMapping(value = "/admin/game/{id}", produces = "application/json")
    public HttpStatus deleteGameBy(@PathVariable long id) {
        if (gameRepository.findById(id).isEmpty()) {
            return HttpStatus.OK;
        }
        gameRepository.deleteById(id);
        if (gameRepository.findById(id).isEmpty()) {
            return HttpStatus.OK;
        }
        return HttpStatus.INTERNAL_SERVER_ERROR;
    }

}
