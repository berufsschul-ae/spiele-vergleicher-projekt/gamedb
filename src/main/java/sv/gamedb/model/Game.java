package sv.gamedb.model;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

import javax.persistence.*;

@Data
@Entity
@Table(name = "games")
public class Game {
    @Setter(AccessLevel.PRIVATE)
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "title")
    private String title;

    @Column(name = "description")
    private String description;

    public Game(long id, String title, String description) {
        this(title, description);
        this.id = id;
    }

    public Game(String title, String description) {
        this.title = title;
        this.description = description;
    }

    public Game() {
    }
}
