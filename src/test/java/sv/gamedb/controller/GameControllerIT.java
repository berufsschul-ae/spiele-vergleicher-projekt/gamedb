package sv.gamedb.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.log4j.Log4j2;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import sv.gamedb.model.Game;
import sv.gamedb.reposetory.GameRepository;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@Log4j2
@SpringBootTest
@AutoConfigureMockMvc
public class GameControllerIT {

    @Autowired
    GameRepository gameRepository;

    @Autowired
    MockMvc mvc;

    private List<Game> games;

    @BeforeEach
    public void setup() {
        gameRepository.deleteAll();
        var games = Arrays.asList(
                new Game("Rainbow Six Siege", "FPS"),
                new Game("League of Legends", "Moba"),
                new Game("UNO", "Card game"),
                new Game("Monopoly", "Classic")
        );

        this.games = gameRepository.saveAll(games);
    }

    @Test
    public void shouldGetAllGames() throws Exception {
        var result = mvc.perform(MockMvcRequestBuilders.get("/games")).andReturn();

        var games = new ObjectMapper().readValue(result.getResponse().getContentAsString(), new TypeReference<List<Game>>() {
        });
        assertEquals(4, games.size());
    }

    @Test
    public void shouldGetAllGamesWithSpecificSize() throws Exception {
        var result = mvc.perform(MockMvcRequestBuilders.get("/games?size=2")).andReturn();

        var games = new ObjectMapper().readValue(result.getResponse().getContentAsString(), new TypeReference<List<Game>>() {
        });
        assertEquals(2, games.size());
    }

    @Test
    public void shouldSearchForAGame() throws Exception {
        var result = mvc.perform(MockMvcRequestBuilders.get("/games/search?title=mono")).andReturn();

        var games = new ObjectMapper().readValue(result.getResponse().getContentAsString(), new TypeReference<List<Game>>() {
        });

        assertEquals("Monopoly", games.get(0).getTitle());
    }

    @Test
    public void shouldGetGameById() throws Exception {
        var id = games.get(0).getId();
        var result = mvc.perform(MockMvcRequestBuilders.get("/game/" + id)).andReturn();

        var game = new ObjectMapper().readValue(result.getResponse().getContentAsString(), Game.class);

        assertEquals(id, game.getId());
    }

    @Test
    public void shouldNotGetGameById() throws Exception {
        var id = 0;
        var result = mvc.perform(MockMvcRequestBuilders.get("/game/" + id)).andReturn();

        assertEquals(HttpStatus.NOT_FOUND.value(), result.getResponse().getStatus());
    }

    @WithMockUser("admin")
    @Test
    public void shouldAddNewGame() throws Exception {
        var result = mvc.perform(MockMvcRequestBuilders.post("/admin/game?title=test&description=test description")).andReturn();

        assertEquals(HttpStatus.CREATED.value(), result.getResponse().getStatus());

        var game = new ObjectMapper().readValue(result.getResponse().getContentAsString(), Game.class);

        assertTrue(gameRepository.findById(game.getId()).isPresent());
        assertEquals(gameRepository.findById(game.getId()).get(), game);
        assertEquals("test", game.getTitle());
    }

    @WithMockUser("admin")
    @Test
    public void shouldUpdateGame() throws Exception {
        var id = games.get(0).getId();
        var result = mvc.perform(MockMvcRequestBuilders.patch("/admin/game/" + id + "?description=Updated description")).andReturn();

        assertEquals(HttpStatus.OK.value(), result.getResponse().getStatus());

        var game = new ObjectMapper().readValue(result.getResponse().getContentAsString(), Game.class);

        assertTrue(gameRepository.findById(game.getId()).isPresent());
        assertEquals(gameRepository.findById(game.getId()).get(), game);
        assertEquals("Updated description", game.getDescription());
    }


    @WithMockUser("admin")
    @Test
    public void shouldNotUpdateGame() throws Exception {
        var id = 0;
        var result = mvc.perform(MockMvcRequestBuilders.patch("/admin/game/" + id + "?description=Updated description")).andReturn();

        assertEquals(HttpStatus.NOT_FOUND.value(), result.getResponse().getStatus());
    }

    @WithMockUser("admin")
    @Test
    public void shouldDeleteGame() throws Exception {
        var id = games.get(0).getId();

        var result = mvc.perform(MockMvcRequestBuilders.delete("/admin/game/" + id)).andReturn();

        assertEquals(HttpStatus.OK.value(), result.getResponse().getStatus());
        assertTrue(gameRepository.findById(id).isEmpty());
    }
}
