package sv.gamedb.controller;

import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import sv.gamedb.model.Game;
import sv.gamedb.reposetory.GameRepository;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class GameControllerTest {

    @Test
    public void shouldGetTenFistGames() {
        var mockGameRepository = mock(GameRepository.class);
        var gameController = new GameController(mockGameRepository);

        var games1 = new ArrayList<Game>();
        for (int i = 0; i < 10; i++) {
            games1.add(new Game(i, "title", "description"));
        }
        var games = (List<Game>) games1;

        var defaultPage = PageRequest.of(0, 10);

        when(mockGameRepository.findAll(defaultPage)).thenReturn(new PageImpl<>(games, defaultPage, 10));

        assertEquals(games, gameController.getGamesBy(0, 10).getBody());
        assertEquals(HttpStatus.OK, gameController.getGamesBy(0, 10).getStatusCode());
    }

    @Test
    public void shouldSearchGame() {
        var mockGameRepository = mock(GameRepository.class);
        var gameController = new GameController(mockGameRepository);
        var game = Collections.singletonList(new Game("test", "description"));


        when(mockGameRepository.findByTitleIgnoreCaseContaining("test")).thenReturn(game);

        assertEquals(game, gameController.searchGamesBy("test").getBody());
        assertEquals(HttpStatus.OK, gameController.searchGamesBy("test").getStatusCode());
    }

    @Nested
    public class GetGameById {
        @Test
        public void shouldGetGameBy() {
            var mockGameRepository = mock(GameRepository.class);
            var gameController = new GameController(mockGameRepository);
            var game = new Game("test", "description");

            when(mockGameRepository.findById(1L)).thenReturn(Optional.of(game));

            assertEquals(game, gameController.getGameBy(1L).getBody());
            assertEquals(HttpStatus.OK, gameController.getGameBy(1L).getStatusCode());
        }

        @Test
        public void shouldErrorCodeIfNotFound() {
            var mockGameRepository = mock(GameRepository.class);
            var gameController = new GameController(mockGameRepository);

            when(mockGameRepository.findById(1L)).thenReturn(Optional.empty());

            assertEquals(HttpStatus.NOT_FOUND, gameController.getGameBy(1L).getStatusCode());
        }
    }

    @Test
    public void shouldCreateNewGame() {
        var mockGameRepository = mock(GameRepository.class);
        var gameController = new GameController(mockGameRepository);

        var game = new Game("title", "description");

        when(mockGameRepository.save(game)).thenReturn(game);
        assertEquals(HttpStatus.CREATED, gameController.addGameBy("title", "description").getStatusCode());
    }

    @Nested
    public class UpdateGame {

        @Test
        public void shouldUpdateGameWithAllParams() {
            var mockGameRepository = mock(GameRepository.class);
            var gameController = new GameController(mockGameRepository);

            var game = new Game();
            var newTitle = "new Title";
            var newDescription = "new Description";
            var newGame = new Game(newTitle, newDescription);

            when(mockGameRepository.findById(1L)).thenReturn(Optional.of(game));
            when(mockGameRepository.save(newGame)).thenReturn(newGame);

            assertEquals(HttpStatus.OK, gameController.updateGame(1L, Optional.of(newTitle), Optional.of(newDescription)).getStatusCode());
        }

        @Test
        public void shouldErrorIfIdNotExists() {
            var mockGameRepository = mock(GameRepository.class);
            var gameController = new GameController(mockGameRepository);
            var emptyString = Optional.of("");

            when(mockGameRepository.findById(1L)).thenReturn(Optional.empty());

            assertEquals(HttpStatus.NOT_FOUND, gameController.updateGame(1L, emptyString, emptyString).getStatusCode());
        }

        @Test
        public void shouldUpdateEvenIfMissingDescription() {
            var mockGameRepository = mock(GameRepository.class);
            var gameController = new GameController(mockGameRepository);

            var game = new Game();
            var newTitle = "new Title";
            var newGame = new Game(newTitle, game.getDescription());

            when(mockGameRepository.findById(1L)).thenReturn(Optional.of(game));
            when(mockGameRepository.save(newGame)).thenReturn(newGame);

            assertEquals(HttpStatus.OK, gameController.updateGame(1L, Optional.of(newTitle), Optional.empty()).getStatusCode());
        }

        @Test
        public void shouldUpdateEvenIfMissingTitle() {
            var mockGameRepository = mock(GameRepository.class);
            var gameController = new GameController(mockGameRepository);

            var game = new Game();
            var newDescription = "new Description";
            var newGame = new Game(game.getDescription(), newDescription);

            when(mockGameRepository.findById(1L)).thenReturn(Optional.of(game));
            when(mockGameRepository.save(newGame)).thenReturn(newGame);

            assertEquals(HttpStatus.OK, gameController.updateGame(1L, Optional.empty(), Optional.of(newDescription)).getStatusCode());
        }

    }

    @Nested
    public class DeleteGame {

        @Test
        public void shouldFailToDeleteAGame() {
            var mockGameRepository = mock(GameRepository.class);
            var gameController = new GameController(mockGameRepository);

            when(mockGameRepository.findById(1L)).thenReturn(Optional.of(new Game()), Optional.empty());

            assertEquals(HttpStatus.OK, gameController.deleteGameBy(1L));
        }

        @Test
        public void shouldDeleteAGame() {
            var mockGameRepository = mock(GameRepository.class);
            var gameController = new GameController(mockGameRepository);

            when(mockGameRepository.findById(1L)).thenReturn(Optional.empty());

            assertEquals(HttpStatus.OK, gameController.deleteGameBy(1L));
        }

        @Test
        public void shouldInternalError() {
            var mockGameRepository = mock(GameRepository.class);
            var gameController = new GameController(mockGameRepository);

            when(mockGameRepository.findById(1L)).thenReturn(Optional.of(new Game()), Optional.of(new Game()));

            assertEquals(HttpStatus.INTERNAL_SERVER_ERROR, gameController.deleteGameBy(1L));
        }
    }

}